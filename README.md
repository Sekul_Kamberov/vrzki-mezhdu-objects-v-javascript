Типове данни, има 7 вградени типа данни в JavaScript:

6 примитивни типа: низ, число, булева, символ, нулева, неопределена
1 съединен тип: обект, с подтипове като обект, масив, функция и т.н.

Обикновено за JavaScript има объркване в именуването още от самото начало, предотвратявайки чисто групиране на вградени обекти: има обединен тип с име на обект, който се състои от много подтипове. Тези подтипове се наричат ​​колективно вградени обекти (вградени модули) или местни обекти (местни) или просто, добре, обекти. Проблемът е, че един от тези естествени обекти също се нарича обект.

И така, всички вградени всъщност са обекти. Всеки от тях е специализиран подтип от общ тип обекти. Най-често използваните вградени са Object, Array, Date, RegExp, Math, Function. Функцията е обект за повикване; Обектите на струните, символите, числата и булите рядко се използват директно, но те са основни при работата с техните примитивни колеги („боксови примитиви“).

JS средата на хоста предоставя много други местни източници, като глобален обект (прозорец в браузър), но те не са част от ядрото на EcmaScript, а по-скоро те са част от отделна спецификация (браузърен модел на обекта, модел на документа на обекта и т.н.)

Списъци на всички вградени обекти на MDN: Стандартни вградени обекти

Вградени обекти

Обектите в JS са колекция от свойства. Свойството е двойка ключ / стойност: ключът е име на свойство и стойността може да бъде всякакъв тип: примитивен (низ, число и т.н.) или съединение (обект, функция, масив и т.н.).

Вградените обекти имат съответните конструкторски функции: Обект, Функция, Масив, Regexp и т.н.

Всички тези конструкторски функции имат прототип: Object.prototype, Function.prototype, Array.prototype, Regexp.prototype и др.
Конструктори и прототипи

Функциите на конструктора, тъй като са обекти, имат свойства. Едно от тях е специално свойство, наречено прототип, което препраща към обект на прототип на обект - функция. От своя страна, самият прототип обект има специално свойство, наречено конструктор, което препраща обратно функцията конструктор.

Обектът на прототипа има много свойства (методи), специфични за неговия тип; например, прототипният обект на функцията на Array е масив и той притежава свойства (методи), които са полезни при работа с масиви (push, pop, indexOf и т.н.).

Един от начините за това е, че всички местни жители са дефинирани от двойка: конструкторска функция и съответния прототип на функцията.

Когато се отнася до конкретен метод, например метод, открит във самата функция Array, уговорката е да се напише Array.from, но когато методът е на обект Array.prototype, той е съкратен като Array # pop.
Прототип верига

Всички обекти имат достъп до собствените си свойства, но освен това те могат да имат достъп и до свойства, открити на други обекти. Този механизъм се реализира чрез специално вътрешно (достъпно само за JS двигателя) свойство, наречено [[прототип]] (вътрешните свойства обикновено се означават в двойни скоби). ES6 стандартизира публична, достъпна отвън версия на тази собственост, __proto__.

Прототип верига
Диаграма 1: Скрийнсейвър на JS двигателя „изглежда зает“

Диаграмата показва някои общи функции на конструктора и техните прототипи. Дебелите червени линии представляват [[прототип]] връзки, т.е. __proto__ препратки. Тези връзки съставят веригата на прототипа.

(Прото) типична конвенция за именуване на JS: има свойство прототип, което препраща прототипни обекти и вътрешно свойство [[прототип]], заедно с обществено свойство __proto__, което включва веригата прототип. Предупреждение: тези два се отнасят до много различни обекти.

По някакъв начин веригата прототип може да се разглежда като колекция от всички връзки [[Prototype]] или по-точно като верига на прототип на даден обект. Например, веригата на прототипа на Function функция, започва със самата функция, продължава до Function.prototype и след това завършва при Object.prototype.

Object.prototype е крайната връзка във веригата на прототипа, той прекратява веригата на прототипа чрез свързване на прототип до null.

    Всички пътища водят до Object.prototype

Прототипната верига се преминава всеки път, когато обект (масив, функция, обект и т.н.) се запитва за свойство; ако обект няма такова свойство, търсенето ще продължи, като следва връзките на прототипа, до следващия обект. И накрая, ако Object.prototype, който винаги е в края на прототипната верига на който и да е обект, няма такова свойство, търсенето приключи (и неопределеното се връща).

Ето защо всички обекти имат достъп до свойства (методи), открити в Object.prototype като toString (), valueOf (), hasOwnProperty (), propertyIsEnumerable () и т.н.

Всичко казано е вярно за първоначалната JS среда, преди да се изпълни всеки потребителски код - нека потребител се намеси и партидата бързо се изравнява, защото тези отношения (свойства) не са неизменни.
Идентифициране на обекти

Функцията на конструктора, като всички функции имат правилно име (име като низ), така че те лесно се разпознават